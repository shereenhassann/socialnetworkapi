import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Float.parseFloat;

public class API {
    private static ArrayList<User> accounts = new ArrayList<>();

    public static void main(String[] args) {

        System.out.println("**Welcome to NSR Social Network API**");
        while(true) {
            System.out.println("\n-----------------------------------------");
            System.out.println("Press 1 to Register. \nPress 2 to Login.\nPress 3 to Exit.");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":     //register
                {
                    System.out.println("Enter username: ");
                    String name = scanner.nextLine();

                    System.out.println("Enter password: ");
                    String pass = scanner.nextLine();

                    System.out.println("Enter email: ");
                    String email = scanner.nextLine();

                    System.out.println("Enter phone number: ");
                    String phone = scanner.nextLine();

                    System.out.println("Enter gender: ");
                    String gender = scanner.nextLine();

                    System.out.println("Enter country: ");
                    String country = scanner.nextLine();

                    User newUser = new User(name, pass, email, phone, gender, country);
                    accounts.add(newUser);
//                    for(int i =0; i<accounts.size(); i++)
//                        System.out.println("Account "+i+": "+accounts.get(i).getUsername());
                    System.out.println("User created successfully.\n\n");
                    break;
                }

                case "2":   //login
                {
                    System.out.println("Enter username: ");
                    String name = scanner.nextLine();
                    User currentUser = new User();
                    System.out.println("Enter password: ");
                    String pass = scanner.nextLine();

                    int foundAccount= findUserAccount(name, pass);
                    if( foundAccount != (-1))   //user found
                    {
                        int id = accounts.get(foundAccount).getUserID();
                        currentUser = getUserById(id);
                        if (currentUser != null) {
                            System.out.println("\n**Welcome " + getUserById(id).getUsername() + "**");
                            currentUser.loggedIn = true;
                        }
                    }else {
                        System.out.println("Can't log in.\nCheck your data and try again.");
                        break;
                    }

                    while(currentUser.isLoggedIn())
                    {
                        System.out.println("\nChoose operation: ");
                        System.out.println("1)Send friend request.");
                        System.out.println("2)Accept friend request from:");
                        if(currentUser.getFriendRequests().size()>0) {
                            for (int i = 0; i < currentUser.getFriendRequests().size(); i++)
                                System.out.println("    2." + (i+1) + ")" + currentUser.getFriendRequests().get(i).getUsername());
                        }
                        System.out.println("3)Upgrade to Premium.");
                        System.out.println("4)My friends List.");
                        System.out.println("5)Log out.");

                        choice = scanner.nextLine();

                        switch(choice.charAt(0))
                        {
                            case '1': {
                                System.out.print("Search for: ");
                                String input = scanner.nextLine();
                                User user = searchForUser(input, currentUser);
                                if(user !=null) {
                                    user.addPermittedUserRequest(currentUser, false);
                                    System.out.println("Request sent successfully.");
                                }
                                else
                                    System.out.println("Request can't be sent.");

                                break;
                            }
                            case '2':
                            {
                                if(choice.length() > 1)
                                {  int reqIndex = Character.getNumericValue(choice.charAt(2)) - 1 ;
                                currentUser.addPermittedUser(currentUser.getFriendRequests().get(reqIndex));
                                currentUser.requests.remove(currentUser.getFriendRequests().get(reqIndex));
                                }
                                else
                                    System.out.println("You have no requests.");
                                break;
                            }
                            case '3':
                            {
                                System.out.print("Enter payment method:\n1)PayPal.\n2)Credit Card.\n>>");
                                String payment = scanner.nextLine();
                                System.out.print("Enter balance:");
                                String balance = scanner.nextLine();
                                if(parseFloat(balance) < 99) {
                                    currentUser.upgradeToPremium(payment, parseFloat(balance));
                                    System.out.println("YAYY!! You're now one of our premium users.");
                                }else
                                    System.out.println("Balance should be more than 99$");
                                break;
                            }
                            case '4':
                            {
                                if(currentUser.permittedUsers.size() > 0) {
                                    for (int i = 0; i < currentUser.permittedUsers.size(); i++)
                                        System.out.println((i+1) + ")" + currentUser.permittedUsers.get(i).getUsername());
                                }
                                else
                                    System.out.println("You have no friends.");

                                break;
                            }
                            case '5'://log out
                            {
                                currentUser.loggedIn = false;
                                break;
                            }

                            default:
                                System.out.println("Invalid Input.");
                        }
                    }

                    break;
                }

                case "3":   //exit
                {
                    System.out.println("**Thank you for using NSR Social Network API**");
                    return;
                }
                default:
                    System.out.println("Invalid input");
            }

        }
    }

    private static Integer findUserAccount(String username, String password)
    {
        int counter = 0;
        for(int i =0; i< accounts.size(); i++) {
            if(accounts.get(i).getUsername().equals(username) && accounts.get(i).getPassword().equals(password))
                return i;
        }
        return -1; //not found
    }

    private static User getUserById(Integer id)
    {
        for(int i =0; i< accounts.size() ; i++) {
            if (accounts.get(i).getUserID() == id)
                return accounts.get(i);
        }
        return null;//not found
    }
    private static User getUserByUsername(String username)
    {
        for(int i =0; i< accounts.size() ; i++) {
          //  System.out.println("func Account "+i+": "+accounts.get(i).getUsername());
            if (accounts.get(i).getUsername().equals(username))
            { //           System.out.println("func Account "+i+": "+accounts.get(i).getUsername()+" == "+username);

                return accounts.get(i);}
        }
        return null; //not found
    }

    private static User searchForUser(String searchStatement, User currentUser) {

        User found = null;
        String searchFor = "", results = "Search Results:\n";
        for (int i = 2; i < searchStatement.length(); i++)
            searchFor += searchStatement.charAt(i);
      //  System.out.println("searchStatement = [" + searchStatement + "], searchFor = [" + searchFor + "]");

        if (searchStatement.charAt(0) == 'u') {
            found = getUserByUsername(searchFor);
            if (found != null)
                results += "Users found: " + found.getUsername();
            else
                results += "No users found.";
        }
        System.out.println(results);
        return found;
    }

}
